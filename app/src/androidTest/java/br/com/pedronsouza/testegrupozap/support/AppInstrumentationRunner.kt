package br.com.pedronsouza.testegrupozap.support

import android.app.Application
import android.content.Context
import androidx.test.runner.AndroidJUnitRunner

class AppInstrumentationRunner : AndroidJUnitRunner() {
    override fun newApplication(cl: ClassLoader?, className: String?, context: Context?): Application {
        return super.newApplication(cl, MockApp::class.java.name, context)
    }
}