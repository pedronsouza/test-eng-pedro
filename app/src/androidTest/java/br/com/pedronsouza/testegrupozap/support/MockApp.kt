package br.com.pedronsouza.testegrupozap.support

import br.com.pedronsouza.testegrupozap.App
import br.com.pedronsouza.testegrupozap.di.DaggerAppComponent
import br.com.pedronsouza.testegrupozap.di.modules.app.PresentersModule
import br.com.pedronsouza.testegrupozap.di.modules.data.DataModule

class MockApp : App() {
    override fun onCreate() {
        super.onCreate()
        appComponent = DaggerAppComponent.builder()
            .dataModule(DataModule("http://localhost:8080"))
            .presentersModule(PresentersModule())
            .build()
    }
}