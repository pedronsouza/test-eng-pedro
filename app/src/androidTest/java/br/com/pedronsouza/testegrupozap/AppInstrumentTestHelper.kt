package br.com.pedronsouza.testegrupozap

import android.view.View
import androidx.test.InstrumentationRegistry
import androidx.test.espresso.UiController
import java.util.Scanner
import androidx.test.espresso.matcher.ViewMatchers.isRoot
import androidx.test.espresso.ViewAction
import br.com.pedronsouza.testegrupozap.support.RecyclerViewMatcher
import org.hamcrest.Matcher


class AppInstrumentTestHelper {
    companion object {
        fun json(fileName: String) : String {
            val inputStream = InstrumentationRegistry.getContext().getResources().getAssets().open(fileName)
            val s = Scanner(inputStream).useDelimiter("\\A")
            return if (s.hasNext()) s.next() else ""
        }

        fun waitFor(millis: Long): ViewAction {
            return object : ViewAction {
                override fun getConstraints(): Matcher<View> {
                    return isRoot()
                }

                override fun getDescription(): String {
                    return "Wait for $millis milliseconds."
                }

                override fun perform(uiController: UiController, view: View) {
                    uiController.loopMainThreadForAtLeast(millis)
                }
            }
        }

        fun withRecyclerView(recyclerViewId: Int): RecyclerViewMatcher {
            return RecyclerViewMatcher(recyclerViewId)
        }
    }
}