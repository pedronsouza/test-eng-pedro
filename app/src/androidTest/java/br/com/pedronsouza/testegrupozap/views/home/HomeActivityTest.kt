package br.com.pedronsouza.testegrupozap.views.home

import android.content.Intent
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.intent.Intents.intended
import androidx.test.espresso.intent.matcher.IntentMatchers.hasComponent
import androidx.test.espresso.intent.matcher.IntentMatchers.hasExtra
import androidx.test.espresso.intent.rule.IntentsTestRule
import androidx.test.espresso.matcher.ViewMatchers.isDisplayed
import androidx.test.espresso.matcher.ViewMatchers.withId
import androidx.test.filters.LargeTest
import androidx.test.rule.ActivityTestRule
import androidx.test.runner.AndroidJUnit4
import br.com.pedronsouza.testegrupozap.AppInstrumentTestHelper
import br.com.pedronsouza.testegrupozap.R
import br.com.pedronsouza.testegrupozap.presentation.entities.PropertyListItemEntity
import br.com.pedronsouza.testegrupozap.presentation.views.home.activities.HomeActivity
import br.com.pedronsouza.testegrupozap.presentation.views.propertyList.activities.PropertyListActivity
import br.com.pedronsouza.testegrupozap.views.BaseInstrumentTest
import okhttp3.mockwebserver.MockResponse
import org.hamcrest.Matchers.allOf
import org.json.JSONArray
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
@LargeTest
class HomeActivityTest : BaseInstrumentTest() {
    val activityRule = ActivityTestRule<HomeActivity>(HomeActivity::class.java)

    @Before
    override fun setUp() {
        super.setUp()
        val i = Intent()
        activityRule.launchActivity(i)
    }

    @Test
    fun shouldContainButtonsForBothCompanies() {
        onView(withId(R.id.seeVivaRealProperties)).check(matches(isDisplayed()))
        onView(withId(R.id.seeGrupoZapProperties)).check(matches(isDisplayed()))
    }

    @Test
    fun shouldRedirectToPropertyListWithVRealProvider() {
        val json = JSONArray(AppInstrumentTestHelper.json("propertyList_fixture.json"))
        val response = MockResponse()
        response.setResponseCode(200)
        response.setBody(json.toString())
        webServer.enqueue(response)

        onView(withId(R.id.seeVivaRealProperties)).perform(click())

        intended(allOf(
            hasComponent(PropertyListActivity::class.java.name),
            hasExtra(PropertyListActivity.PROVIDER_KEY, PropertyListItemEntity.PropertyProvider.VREAL)
        ))
    }

    @Test
    fun shouldRedirectToPropertyListWithZapProvider() {
        val json = JSONArray(AppInstrumentTestHelper.json("propertyList_fixture.json"))
        val response = MockResponse()
        response.setResponseCode(200)
        response.setBody(json.toString())
        webServer.enqueue(response)

        onView(withId(R.id.seeVivaRealProperties)).perform(click())

        intended(allOf(
            hasComponent(PropertyListActivity::class.java.name),
            hasExtra(PropertyListActivity.PROVIDER_KEY, PropertyListItemEntity.PropertyProvider.VREAL)
        ))
    }
}