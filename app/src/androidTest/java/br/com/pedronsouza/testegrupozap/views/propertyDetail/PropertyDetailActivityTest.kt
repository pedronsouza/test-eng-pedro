package br.com.pedronsouza.testegrupozap.views.propertyDetail

import android.content.Intent
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.*
import androidx.test.filters.LargeTest
import androidx.test.rule.ActivityTestRule
import androidx.test.runner.AndroidJUnit4
import br.com.pedronsouza.testegrupozap.AppInstrumentTestHelper
import br.com.pedronsouza.testegrupozap.R
import br.com.pedronsouza.testegrupozap.domain.models.Property
import br.com.pedronsouza.testegrupozap.presentation.entities.PropertyListItemEntity
import br.com.pedronsouza.testegrupozap.presentation.views.propertyDetail.activities.PropertyDetailActivity
import br.com.pedronsouza.testegrupozap.views.BaseInstrumentTest
import org.json.JSONArray
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
@LargeTest
class PropertyDetailActivityTest : BaseInstrumentTest() {
    @get:Rule
    var activityRule = ActivityTestRule(PropertyDetailActivity::class.java, false, false)
    lateinit var property : PropertyListItemEntity

    @Before
    override fun setUp() {
        super.setUp()
        val json = JSONArray(AppInstrumentTestHelper.json("propertyList_fixture.json"))
        val p = gson.fromJson(json.getJSONObject(0).toString(), Property::class.java)
        property = PropertyListItemEntity.from(p)

        val i = Intent()
        i.putExtra(PropertyDetailActivity.PROPERTY_DETAIL_ITEM_KEY, property)
        activityRule.launchActivity(i)
    }

    @Test
    fun shouldDisplayPropertyInfo() {
        onView(withId(R.id.propertyTitle)).check(matches(isDisplayed()))
        onView(withId(R.id.subtitle)).check(matches(isDisplayed()))
        onView(withId(R.id.price)).check(matches(isDisplayed()))

        onView(withText(property.getTitle())).check(matches(isDisplayed()))
        onView(withText(property.getSubTitle())).check(matches(isDisplayed()))
        onView(withText(property.getFormatPrice())).check(matches(isDisplayed()))
    }

}