package br.com.pedronsouza.testegrupozap.views.propertyList

import android.content.Intent
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.assertion.ViewAssertions.doesNotExist
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.intent.Intents.intended
import androidx.test.espresso.intent.matcher.IntentMatchers.hasComponent
import androidx.test.espresso.intent.rule.IntentsTestRule
import androidx.test.espresso.matcher.ViewMatchers.*
import androidx.test.filters.LargeTest
import androidx.test.rule.ActivityTestRule
import androidx.test.runner.AndroidJUnit4
import br.com.pedronsouza.testegrupozap.AppInstrumentTestHelper
import br.com.pedronsouza.testegrupozap.AppInstrumentTestHelper.Companion.withRecyclerView
import br.com.pedronsouza.testegrupozap.R
import br.com.pedronsouza.testegrupozap.domain.models.Property
import br.com.pedronsouza.testegrupozap.presentation.entities.PropertyListItemEntity
import br.com.pedronsouza.testegrupozap.presentation.views.propertyDetail.activities.PropertyDetailActivity
import br.com.pedronsouza.testegrupozap.presentation.views.propertyList.activities.PropertyListActivity
import br.com.pedronsouza.testegrupozap.views.BaseInstrumentTest
import okhttp3.mockwebserver.MockResponse
import org.json.JSONArray
import org.junit.Before
import org.junit.Ignore
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
@LargeTest
class PropertyListActivityTest : BaseInstrumentTest() {
    @get:Rule
    var activityRule = ActivityTestRule(PropertyListActivity::class.java, false, false)

    var json : JSONArray? = null

    @Before
    override fun setUp() {
        super.setUp()
    }

    private fun enqueueMockResponse(response : MockResponse? = null) {
        if (response == null) {
            val res = MockResponse()
            json = JSONArray(AppInstrumentTestHelper.json("propertyList_fixture.json"))

            res.setResponseCode(200)
            res.setBody(json.toString())
            webServer.enqueue(res)
        } else {
            webServer.enqueue(response)
        }


    }

    @Test
    fun shouldLoadItemsOnScreen() {
        enqueueMockResponse()
        val i = Intent()
        i.putExtra(PropertyListActivity.PROVIDER_KEY, PropertyListItemEntity.PropertyProvider.VREAL)
        activityRule.launchActivity(i)

        val entity = activityRule.activity.presenter.items.first()

        onView(withId(R.id.propertiesList)).check(matches(isDisplayed()))
        onView(withRecyclerView(R.id.propertiesList).atPosition(0))
            .check(
                matches(
                    hasDescendant(
                        withText(entity.getTitle()))
                    )
            )
    }

    @Test
    fun shouldRedirectToDetailsScreen() {
        enqueueMockResponse()
        val i = Intent()
        i.putExtra(PropertyListActivity.PROVIDER_KEY, PropertyListItemEntity.PropertyProvider.VREAL)

        activityRule.launchActivity(i)
        onView(withRecyclerView(R.id.propertiesList).atPosition(0)).perform(click())

        intended(hasComponent(PropertyDetailActivity::class.java.name))
    }

    @Test
    fun shouldShowNothingWithInvalidPropertyForZap() {
        val i = Intent()
        i.putExtra(PropertyListActivity.PROVIDER_KEY, PropertyListItemEntity.PropertyProvider.ZAP)
        val response = MockResponse()
        val invalidProperty = gson.fromJson(AppInstrumentTestHelper.json("invalid_zap_property.json"), Property::class.java)
        val property = PropertyListItemEntity.from(invalidProperty)

        response.setBody(gson.toJson(arrayOf(invalidProperty)))

        enqueueMockResponse(response)
        activityRule.launchActivity(i)

        onView(withText(property.getTitle())).check(doesNotExist())

    }

    @Test
    fun shouldShowNothingWithInvalidPropertyForVReal() {
        val i = Intent()
        i.putExtra(PropertyListActivity.PROVIDER_KEY, PropertyListItemEntity.PropertyProvider.VREAL)
        val response = MockResponse()
        val invalidProperty = gson.fromJson(AppInstrumentTestHelper.json("invalid_vreal_property.json"), Property::class.java)
        val property = PropertyListItemEntity.from(invalidProperty)

        response.setBody(gson.toJson(arrayOf(invalidProperty)))
        enqueueMockResponse(response)

        activityRule.launchActivity(i)
        onView(withText(property.getTitle())).check(doesNotExist())
    }

    @Test
    fun shouldShowAlertDialogInCaseOfError() {
        val i = Intent()
        i.putExtra(PropertyListActivity.PROVIDER_KEY, PropertyListItemEntity.PropertyProvider.VREAL)
        val response = MockResponse()
        response.setResponseCode(404)
        response.setBody("{}")
        enqueueMockResponse(response)

        activityRule.launchActivity(i)
        onView(withText(R.string.alert_property_list_error)).check(matches(isDisplayed()))
    }
}