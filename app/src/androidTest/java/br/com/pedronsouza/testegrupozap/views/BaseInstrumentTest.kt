package br.com.pedronsouza.testegrupozap.views


import androidx.test.espresso.intent.Intents
import androidx.test.runner.AndroidJUnit4
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import okhttp3.mockwebserver.MockWebServer
import org.junit.After
import org.junit.Before
import org.junit.runner.RunWith


@RunWith(AndroidJUnit4::class)
abstract class BaseInstrumentTest {
    protected lateinit var webServer: MockWebServer
    protected var gson : Gson = GsonBuilder().create()

    @Before
    open fun setUp() {
        Intents.init()
        webServer = MockWebServer()
        webServer.start(8080)
    }

    @After
    open fun tearDown() {
        Intents.release()
        webServer.shutdown()
    }
}