package br.com.pedronsouza.testegrupozap.presentation.views.propertyList.dataSources

import android.net.Uri
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.widget.AppCompatTextView
import androidx.paging.PagedListAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import br.com.pedronsouza.testegrupozap.R
import br.com.pedronsouza.testegrupozap.presentation.entities.PropertyListItemEntity
import com.facebook.drawee.view.SimpleDraweeView
import io.reactivex.subjects.PublishSubject

class PropertyListAdapter(val clickItemSubject: PublishSubject<PropertyListItemEntity>) : PagedListAdapter<PropertyListItemEntity, PropertyListAdapter.PropertyListItemViewHolder>(UserDiffCallback) {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PropertyListItemViewHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.view_property_list_item, parent, false)
        return PropertyListItemViewHolder(v)

    }

    override fun onBindViewHolder(holder: PropertyListItemViewHolder, position: Int) {
        holder.update(getItem(position))
    }

    inner class PropertyListItemViewHolder(itemView : View) : RecyclerView.ViewHolder(itemView) {
        private val thumb       by lazy { itemView.findViewById<SimpleDraweeView>(R.id.thumb) }
        private val title       by lazy { itemView.findViewById<AppCompatTextView>(R.id.title) }
        private val subTitle    by lazy { itemView.findViewById<AppCompatTextView>(R.id.subtitle) }
        private val price       by lazy { itemView.findViewById<AppCompatTextView>(R.id.price) }

        fun update(item : PropertyListItemEntity?) {
            if (item != null) {
                thumb.setImageURI(Uri.parse(item?.images?.get(0)))
                title.text = item.getTitle()
                subTitle.text = item.getSubTitle()
                price.text = item.getFormatPrice()
                itemView.setOnClickListener { clickItemSubject.onNext(item) }
            }
        }
    }

    companion object {
        val UserDiffCallback = object : DiffUtil.ItemCallback<PropertyListItemEntity>() {
            override fun areItemsTheSame(oldItem: PropertyListItemEntity, newItem: PropertyListItemEntity): Boolean {
                return oldItem.id == newItem.id
            }

            override fun areContentsTheSame(oldItem: PropertyListItemEntity, newItem: PropertyListItemEntity): Boolean {
                return oldItem == newItem
            }
        }
    }
}