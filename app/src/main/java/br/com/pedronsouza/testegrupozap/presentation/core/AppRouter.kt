package br.com.pedronsouza.testegrupozap.presentation.core

import android.content.Intent
import br.com.pedronsouza.testegrupozap.presentation.entities.PropertyListItemEntity
import br.com.pedronsouza.testegrupozap.presentation.views.propertyList.activities.PropertyListActivity
import br.com.pedronsouza.testegrupozap.presentation.views.propertyDetail.activities.PropertyDetailActivity

class AppRouter {
    companion object {
        private var activity : BaseActivity? = null
        private var instance : AppRouter? = null

        fun with(activity: BaseActivity) : AppRouter {
            this.activity = activity

            if (instance == null) {
                instance = AppRouter()
            }

            return instance!!
        }

        fun dispose() {
            activity = null
        }
    }

    fun toPropertyList(provider : PropertyListItemEntity.PropertyProvider) : Intent {
        val i = Intent(activity, PropertyListActivity::class.java)
        i.putExtra(PropertyListActivity.PROVIDER_KEY, provider)
        return i
    }

    fun toPropertyDetail(property : PropertyListItemEntity) : Intent {
        val i =  Intent(activity, PropertyDetailActivity::class.java)
        i.putExtra(PropertyDetailActivity.PROPERTY_DETAIL_ITEM_KEY, property)
        return i
    }
}