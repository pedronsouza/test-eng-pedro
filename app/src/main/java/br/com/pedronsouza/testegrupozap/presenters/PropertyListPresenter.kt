package br.com.pedronsouza.testegrupozap.presenters

import br.com.pedronsouza.testegrupozap.data.usecases.properties.FetchVRealPropertiesUseCase
import br.com.pedronsouza.testegrupozap.data.usecases.properties.FetchZapPropertiesUseCase
import br.com.pedronsouza.testegrupozap.domain.app.presenters.BasePresenter
import br.com.pedronsouza.testegrupozap.domain.models.Property
import br.com.pedronsouza.testegrupozap.presentation.entities.PropertyListItemEntity
import br.com.pedronsouza.testegrupozap.presentation.views.propertyList.PropertyListView
import io.reactivex.Observable

class PropertyListPresenter(private val fetchZapPropertiesUseCase: FetchZapPropertiesUseCase,
                            private val fetchVRealPropertiesUseCase: FetchVRealPropertiesUseCase) : BasePresenter<PropertyListView>() {
    companion object {
        const val pageSize = 20
    }

    var items : List<PropertyListItemEntity> = emptyList()
    lateinit var provider : PropertyListItemEntity.PropertyProvider

    private val onPropertiesFetched : (List<Property>) -> List<PropertyListItemEntity> = {
        items = it.map {property->
            PropertyListItemEntity(property.id,
                property.usableAreas,
                property.pricingInfos.businessType,
                property.pricingInfos.price,
                property.pricingInfos.monthlyCondoFee,
                arrayOf(property.address.geoLocation.location.lat, property.address.geoLocation.location.lon),
                property.bedrooms,
                property.bathrooms,
                property.images,
                property.parkingSpaces)

        }

        items
    }

    val onFetchError : (Throwable) -> Unit = {
        this.view.onPropertiesProcessingError()
    }

    private fun fetchProperties() : Observable<List<PropertyListItemEntity>> {
        val observable = if (provider == PropertyListItemEntity.PropertyProvider.ZAP) {
            fetchZapPropertiesUseCase.observableFor()
        } else {
            fetchVRealPropertiesUseCase.observableFor()
        }

        return observable.flatMap { Observable.just(onPropertiesFetched(it)) }
    }

    fun getPage(page : Int) : Observable<List<PropertyListItemEntity>> {
        val action: (List<PropertyListItemEntity>) -> Observable<List<PropertyListItemEntity>> = {
            var start = (page * 1) * pageSize
            var skip = (start + pageSize)

            if (skip >= it.size) {
                skip = it.size - 1
                if (skip < 0) {
                    skip = 0
                    start = 0
                }
            }

            Observable.just(it.subList(start, skip)).doOnError(onFetchError)
        }

        return if (items.isEmpty()) {
            fetchProperties().flatMap(action)
        } else {
            action(items)
        }
    }

    override fun onViewCreated(view: PropertyListView) {
        super.onViewCreated(view)
        fetchVRealPropertiesUseCase.onError = onFetchError
        fetchZapPropertiesUseCase.onError = onFetchError
    }

    override fun dispose() {
        super.dispose()
        fetchZapPropertiesUseCase.dispose()
        fetchVRealPropertiesUseCase.dispose()
        disposableBag.dispose()
    }


}