package br.com.pedronsouza.testegrupozap.presentation.views.home.activities

import br.com.pedronsouza.testegrupozap.R
import br.com.pedronsouza.testegrupozap.presentation.core.AppRouter
import br.com.pedronsouza.testegrupozap.presentation.core.BaseActivity
import br.com.pedronsouza.testegrupozap.presentation.entities.PropertyListItemEntity
import br.com.pedronsouza.testegrupozap.presentation.views.home.HomeView
import com.jakewharton.rxbinding2.view.RxView
import io.reactivex.functions.Consumer
import kotlinx.android.synthetic.main.activity_home.*

class HomeActivity : BaseActivity(), HomeView {
    override fun layoutResourceId(): Int = R.layout.activity_home

    override fun getTitleForActionBar(): String = getString(R.string.home_title)

    private val onVivaRealClicked : Consumer<Any> = Consumer {
        val route = AppRouter.with(this@HomeActivity).toPropertyList(PropertyListItemEntity.PropertyProvider.VREAL)
        startActivity(route)
    }

    private val onZapClicked : Consumer<Any> = Consumer {
        val route = AppRouter.with(this@HomeActivity).toPropertyList(PropertyListItemEntity.PropertyProvider.ZAP)
        startActivity(route)
    }

    override fun onViewsBinded() {
        disposableBag.add(RxView.clicks(seeVivaRealProperties).subscribe(onVivaRealClicked))
        disposableBag.add(RxView.clicks(seeGrupoZapProperties).subscribe(onZapClicked))
    }
}