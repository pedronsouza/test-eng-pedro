package br.com.pedronsouza.testegrupozap.di.modules.data

import br.com.pedronsouza.testegrupozap.BuildConfig
import br.com.pedronsouza.testegrupozap.data.net.Api
import dagger.Module
import dagger.Provides
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton

@Module(includes = [UseCasesModule::class, DataSourcesModule::class])
open class DataModule(val baseUrl : String) {
    @Provides
    @Singleton
    fun provideApi(retrofit: Retrofit) : Api {
        return Api(retrofit)
    }

    @Provides
    open fun provideRetrofit(okHttpClient: OkHttpClient) : Retrofit {
            return Retrofit.Builder()
            .client(okHttpClient)
            .baseUrl(baseUrl)
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .addConverterFactory(GsonConverterFactory.create())
            .build()
    }

    @Provides
    fun provideOkHttpClient(httpLoggingInterceptor: HttpLoggingInterceptor) : OkHttpClient {
        return OkHttpClient
                .Builder()
                .addInterceptor(httpLoggingInterceptor)
                .build()
    }

    @Provides
    fun providesLogginInterceptor() : HttpLoggingInterceptor {
        val interceptor = HttpLoggingInterceptor()
        interceptor.level = if (BuildConfig.DEBUG) HttpLoggingInterceptor.Level.BODY else HttpLoggingInterceptor.Level.NONE
        return interceptor
    }
}