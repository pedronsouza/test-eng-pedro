package br.com.pedronsouza.testegrupozap.presentation.core

import android.os.Bundle
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.LifecycleRegistry

import br.com.pedronsouza.testegrupozap.domain.app.views.BaseView
import io.reactivex.disposables.CompositeDisposable

abstract class BaseActivity : AppCompatActivity(), BaseView, LifecycleOwner {
    protected lateinit var mLifecycleRegistry : LifecycleRegistry
    protected var disposableBag : CompositeDisposable = CompositeDisposable()
    abstract fun layoutResourceId() : Int

    override fun getLifecycle(): Lifecycle {
        return mLifecycleRegistry
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mLifecycleRegistry = LifecycleRegistry(this)
        setContentView(layoutResourceId())
        onViewsBinded()
        supportActionBar?.title = getTitleForActionBar()
    }

    protected fun setAsBackActivity() {
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowHomeEnabled(true)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if (item?.itemId == android.R.id.home) {
            onBackPressed()
            return true
        }

        return false
    }

    override fun onSaveInstanceState(outState: Bundle) {
        mLifecycleRegistry.markState(Lifecycle.State.CREATED)
        super.onSaveInstanceState(outState)
    }

    override fun onStart() {
        mLifecycleRegistry.markState(Lifecycle.State.STARTED)
        super.onStart()
    }

    override fun onResume() {
        mLifecycleRegistry.markState(Lifecycle.State.RESUMED)
        super.onResume()
    }

    override fun onStop() {
        super.onStop()
        mLifecycleRegistry.markState(Lifecycle.State.DESTROYED)
        dispose()
    }

    override fun getBag() =
        disposableBag

    override fun dispose() {
        disposableBag
        AppRouter.dispose()
    }
}