package br.com.pedronsouza.testegrupozap.presentation.views.propertyDetail.activities

import br.com.pedronsouza.testegrupozap.R
import br.com.pedronsouza.testegrupozap.presentation.core.BaseActivity
import br.com.pedronsouza.testegrupozap.presentation.entities.PropertyListItemEntity
import br.com.pedronsouza.testegrupozap.presentation.views.propertyDetail.activities.adapter.ImageSliderAdapter
import kotlinx.android.synthetic.main.activity_property_detail.*

class PropertyDetailActivity : BaseActivity() {
    companion object {
        const val PROPERTY_DETAIL_ITEM_KEY = "PROPERTY_DETAIL_ITEM_KEY"
    }

    private val property by lazy { intent.extras.getParcelable(PROPERTY_DETAIL_ITEM_KEY) as PropertyListItemEntity }

    override fun getTitleForActionBar(): String = getString(R.string.property_detail_title)
    override fun layoutResourceId(): Int = R.layout.activity_property_detail
    override fun onViewsBinded() {
        imageSlider.sliderAdapter = ImageSliderAdapter(property.images)
        propertyTitle.text = property.getTitle()
        subtitle.text = property.getSubTitle()
        price.text = property.getFormatPrice()
        setAsBackActivity()
    }
}