package br.com.pedronsouza.testegrupozap.presentation.entities

import android.os.Parcel
import android.os.Parcelable
import br.com.pedronsouza.testegrupozap.domain.models.BusinessTypeEnum
import br.com.pedronsouza.testegrupozap.domain.models.Property
import java.text.NumberFormat
import java.util.*

data class PropertyListItemEntity(val id : String,
                                  val usableAreas : Int,
                                  val businessType : BusinessTypeEnum,
                                  val price : Double,
                                  val monthlyCondoFee : Double,
                                  val locations : Array<Double>,
                                  val bedrooms : Int,
                                  val bathrooms: Int,
                                  val images : List<String>,
                                  val parkingSpaces : Int) : Parcelable {
    constructor(parcel: Parcel) : this(
        parcel.readString(),
        parcel.readInt(),
        parcel.readSerializable() as BusinessTypeEnum,
        parcel.readDouble(),
        parcel.readDouble(),
        parcel.createDoubleArray().toTypedArray(),
        parcel.readInt(),
        parcel.readInt(),
        parcel.createStringArrayList().toList(),
        parcel.readInt()
    )


    fun getTitle() : String =
        "Apartamento para ${if (this.businessType == BusinessTypeEnum.SALE)  "Venda" else "Aluguel"}"

    fun getSubTitle() : String =
        "${this.bedrooms} quartos | ${this.bathrooms} banheiros | ${this.parkingSpaces} vagas"

    fun getFormatPrice() : String {
        val formatter   = NumberFormat.getCurrencyInstance(Locale.getDefault())
        return "Valor: R$ ${formatter.format(this.price)}"
    }


    override fun writeToParcel(dest: Parcel?, flags: Int) {
        dest?.writeString(id)
        dest?.writeInt(usableAreas)
        dest?.writeSerializable(businessType)
        dest?.writeDouble(price)
        dest?.writeDouble(monthlyCondoFee)
        dest?.writeDoubleArray(locations.toDoubleArray())
        dest?.writeInt(bedrooms)
        dest?.writeInt(bathrooms)
        dest?.writeStringArray(images.toTypedArray())
        dest?.writeInt(parkingSpaces)
    }

    override fun describeContents(): Int = 0

    enum class PropertyProvider {
        VREAL,
        ZAP
    }

    override fun equals(other: Any?): Boolean {
        return (other as PropertyListItemEntity?)?.id == id
    }

    override fun hashCode(): Int {
        return super.hashCode()
    }

    companion object CREATOR : Parcelable.Creator<PropertyListItemEntity> {
        override fun createFromParcel(parcel: Parcel): PropertyListItemEntity {
            return PropertyListItemEntity(parcel)
        }

        override fun newArray(size: Int): Array<PropertyListItemEntity?> {
            return arrayOfNulls(size)
        }

        fun from(property: Property) : PropertyListItemEntity {
            return PropertyListItemEntity(property.id,
                property.usableAreas,
                property.pricingInfos.businessType,
                property.pricingInfos.price,
                property.pricingInfos.monthlyCondoFee,
                arrayOf(property.address.geoLocation.location.lat, property.address.geoLocation.location.lon),
                property.bedrooms,
                property.bathrooms,
                property.images,
                property.parkingSpaces)
        }
    }
}