package br.com.pedronsouza.testegrupozap

import android.app.Application
import br.com.pedronsouza.testegrupozap.di.AppComponent
import br.com.pedronsouza.testegrupozap.di.DaggerAppComponent
import br.com.pedronsouza.testegrupozap.di.modules.app.PresentersModule
import br.com.pedronsouza.testegrupozap.di.modules.data.DataModule
import com.facebook.drawee.backends.pipeline.Fresco

open class App : Application() {
    companion object {
        lateinit var appComponent: AppComponent
    }
    override fun onCreate() {
        super.onCreate()
        Fresco.initialize(this)
        appComponent = DaggerAppComponent
            .builder()
            .dataModule(DataModule(getString(R.string.base_url)))
            .presentersModule(PresentersModule())
            .build()
    }
}