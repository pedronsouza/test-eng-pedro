package br.com.pedronsouza.testegrupozap.presentation.views.propertyDetail.activities.adapter

import android.net.Uri
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import br.com.pedronsouza.testegrupozap.R
import com.facebook.drawee.view.SimpleDraweeView
import com.smarteist.autoimageslider.SliderViewAdapter

class ImageSliderAdapter(val items : List<String>) : SliderViewAdapter<ImageSliderAdapter.ImageSliderViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup?): ImageSliderViewHolder =
        ImageSliderViewHolder.create(LayoutInflater.from(parent?.context), parent)

    override fun onBindViewHolder(viewHolder: ImageSliderViewHolder?, position: Int) {
        viewHolder?.bind(items[position])
    }

    override fun getCount(): Int = items.size

    class ImageSliderViewHolder(view : View) : SliderViewAdapter.ViewHolder(view) {
        private val imageView by lazy { view.findViewById<SimpleDraweeView>(R.id.imageView) }

        companion object {
            fun create(inflater: LayoutInflater, parent: ViewGroup?) : ImageSliderViewHolder {
                val v = inflater.inflate(R.layout.view_image_slider_view, parent, false)
                return ImageSliderViewHolder(v)
            }
        }

        fun bind(imageUrl : String) =
            imageView.setImageURI(Uri.parse(imageUrl))
    }
}