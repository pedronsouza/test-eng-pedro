package br.com.pedronsouza.testegrupozap.presentation.views.propertyList.dataSources

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import androidx.paging.LivePagedListBuilder
import androidx.paging.PagedList
import br.com.pedronsouza.testegrupozap.presentation.entities.PropertyListItemEntity
import br.com.pedronsouza.testegrupozap.presentation.views.propertyList.PropertyListView
import br.com.pedronsouza.testegrupozap.presenters.PropertyListPresenter
import io.reactivex.disposables.CompositeDisposable

class PropertyListViewModel : ViewModel() {
    private val viewModelBag : CompositeDisposable = CompositeDisposable()
    lateinit var properties : LiveData<PagedList<PropertyListItemEntity>>
    lateinit var sourceFactory : PropertyListFactory

    fun bind(view : PropertyListView) {
        sourceFactory = PropertyListFactory(view, viewModelBag)
        val config = PagedList.Config
            .Builder()
            .setPageSize((PropertyListPresenter.pageSize / 10))
            .setInitialLoadSizeHint((PropertyListPresenter.pageSize * 2))
            .setEnablePlaceholders(false)
            .build()

        properties = LivePagedListBuilder(sourceFactory, config).build()
    }

    override fun onCleared() {
        super.onCleared()
        viewModelBag.dispose()
    }


}