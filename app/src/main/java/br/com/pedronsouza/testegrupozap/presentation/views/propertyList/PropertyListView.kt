package br.com.pedronsouza.testegrupozap.presentation.views.propertyList

import br.com.pedronsouza.testegrupozap.domain.app.views.BaseView
import br.com.pedronsouza.testegrupozap.presentation.entities.PropertyListItemEntity
import io.reactivex.Observable

interface PropertyListView : BaseView {
    fun getProperties(page : Int) : Observable<List<PropertyListItemEntity>>
    fun getErrorHandler() : (Throwable) -> Unit
    fun onPropertiesProcessingError()
}