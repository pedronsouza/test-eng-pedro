package br.com.pedronsouza.testegrupozap.di.modules.data

import br.com.pedronsouza.testegrupozap.data.datasources.PropertiesDataSource
import br.com.pedronsouza.testegrupozap.data.net.Api
import dagger.Module
import dagger.Provides

@Module
class DataSourcesModule {
    @Provides
    fun providePropertiesDataSource(api : Api) : PropertiesDataSource {
        return PropertiesDataSource(api)
    }
}