package br.com.pedronsouza.testegrupozap.di.modules.app

import br.com.pedronsouza.testegrupozap.data.usecases.properties.FetchVRealPropertiesUseCase
import br.com.pedronsouza.testegrupozap.data.usecases.properties.FetchZapPropertiesUseCase
import br.com.pedronsouza.testegrupozap.presenters.PropertyListPresenter
import dagger.Module
import dagger.Provides

@Module
class PresentersModule {

    @Provides
    fun providePropertyListPresenter(fetchZapUseCase : FetchZapPropertiesUseCase,
                                     fetchVRealUseCase : FetchVRealPropertiesUseCase) : PropertyListPresenter =
        PropertyListPresenter(fetchZapUseCase, fetchVRealUseCase)

}