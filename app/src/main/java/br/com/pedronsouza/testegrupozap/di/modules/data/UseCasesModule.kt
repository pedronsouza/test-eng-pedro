package br.com.pedronsouza.testegrupozap.di.modules.data

import br.com.pedronsouza.testegrupozap.data.datasources.PropertiesDataSource
import br.com.pedronsouza.testegrupozap.data.usecases.properties.FetchAvailablePropertiesUseCase
import br.com.pedronsouza.testegrupozap.data.usecases.properties.FetchVRealPropertiesUseCase
import br.com.pedronsouza.testegrupozap.data.usecases.properties.FetchZapPropertiesUseCase
import dagger.Module
import dagger.Provides

@Module
class UseCasesModule {
    @Provides
    fun providesFetchVRealPropertiesUseCase(dataSource: PropertiesDataSource) : FetchVRealPropertiesUseCase {
        return FetchVRealPropertiesUseCase(dataSource)
    }

    @Provides
    fun providesFetchZapPropertiesUseCase(dataSource: PropertiesDataSource) : FetchZapPropertiesUseCase {
        return FetchZapPropertiesUseCase(dataSource)
    }
}