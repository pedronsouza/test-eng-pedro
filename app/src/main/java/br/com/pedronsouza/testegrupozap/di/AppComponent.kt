package br.com.pedronsouza.testegrupozap.di

import br.com.pedronsouza.testegrupozap.data.datasources.PropertiesDataSource
import br.com.pedronsouza.testegrupozap.data.net.Api
import br.com.pedronsouza.testegrupozap.data.usecases.properties.FetchVRealPropertiesUseCase
import br.com.pedronsouza.testegrupozap.data.usecases.properties.FetchZapPropertiesUseCase
import br.com.pedronsouza.testegrupozap.di.modules.app.PresentersModule
import br.com.pedronsouza.testegrupozap.di.modules.data.DataModule
import br.com.pedronsouza.testegrupozap.presentation.views.propertyList.activities.PropertyListActivity
import br.com.pedronsouza.testegrupozap.presenters.PropertyListPresenter
import dagger.Component
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import javax.inject.Singleton

@Singleton
@Component(modules = [DataModule::class, PresentersModule::class])
interface AppComponent {
    fun retrofit() : Retrofit
    fun okHttp() : OkHttpClient
    fun api() : Api
    fun propertiesDataSource() : PropertiesDataSource
    fun propertyListPresenter() : PropertyListPresenter
    fun logginInterceptor() : HttpLoggingInterceptor

    fun inject(view : PropertyListActivity)
}