package br.com.pedronsouza.testegrupozap.support

import androidx.annotation.CheckResult
import androidx.annotation.NonNull
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import io.reactivex.Observable
import io.reactivex.functions.Consumer


class RxSwipeRefreshLayout private constructor() {

    init {
        throw AssertionError("No instances.")
    }

    companion object {
        /**
         * Create an observable of refresh events on `view`.
         *
         *
         * *Warning:* The created observable keeps a strong reference to `view`. Unsubscribe
         * to free this reference.
         */
        @CheckResult
        @NonNull
        fun refreshes(
            @NonNull view: SwipeRefreshLayout
        ): Observable<Any> {

            return SwipeRefreshLayoutRefreshObservable(view)
        }

        /**
         * An action which sets whether the layout is showing the refreshing indicator.
         *
         *
         * *Warning:* The created observable keeps a strong reference to `view`. Unsubscribe
         * to free this reference.
         */
        @CheckResult
        @NonNull
        fun refreshing(
            @NonNull view: SwipeRefreshLayout
        ): Consumer<in Boolean> {

            return object : Consumer<Boolean> {
                override fun accept(value: Boolean?) {
                    view.isRefreshing = value!!
                }
            }
        }
    }
}