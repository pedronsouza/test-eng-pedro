package br.com.pedronsouza.testegrupozap.presentation.views.propertyList.activities

import androidx.appcompat.app.AlertDialog
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.paging.PagedList
import br.com.pedronsouza.testegrupozap.App
import br.com.pedronsouza.testegrupozap.R
import br.com.pedronsouza.testegrupozap.presentation.core.AppRouter
import br.com.pedronsouza.testegrupozap.presentation.core.BaseActivity
import br.com.pedronsouza.testegrupozap.presentation.entities.PropertyListItemEntity
import br.com.pedronsouza.testegrupozap.presentation.views.propertyList.PropertyListView
import br.com.pedronsouza.testegrupozap.presentation.views.propertyList.dataSources.PropertyListAdapter
import br.com.pedronsouza.testegrupozap.presentation.views.propertyList.dataSources.PropertyListViewModel
import br.com.pedronsouza.testegrupozap.presenters.PropertyListPresenter
import br.com.pedronsouza.testegrupozap.support.RxSwipeRefreshLayout
import io.reactivex.Observable
import io.reactivex.functions.Consumer
import io.reactivex.subjects.PublishSubject
import kotlinx.android.synthetic.main.activity_property_list.*
import javax.inject.Inject

class PropertyListActivity : BaseActivity(), PropertyListView, LifecycleOwner{
    companion object {
        const val PROVIDER_KEY = "PROVIDER_KEY"
    }
    @Inject lateinit var presenter : PropertyListPresenter

    private var adapter : PropertyListAdapter? = null
    private lateinit var viewModel : PropertyListViewModel
    private val provider by lazy { intent?.extras?.getSerializable(PROVIDER_KEY) as PropertyListItemEntity.PropertyProvider }

    override fun getErrorHandler(): (Throwable) -> Unit =
        presenter.onFetchError

    override fun onViewsBinded() {
        App.appComponent.inject(this)
        presenter.onViewCreated(this)
        presenter.provider = provider

        disposableBag.add(RxSwipeRefreshLayout.refreshes(refreshLayout).subscribe(onRefreshesCalled))
        disposableBag.add(onPropertyItemClickedSubject.subscribe(onPropertyItemClicked))

        RxSwipeRefreshLayout.refreshing(refreshLayout).accept(true)
        initList()
        setAsBackActivity()
    }

    private fun initList() {
        if (adapter == null) {
            adapter = PropertyListAdapter(onPropertyItemClickedSubject)

            viewModel = ViewModelProviders.of(this).get(PropertyListViewModel::class.java)
            viewModel.bind(this)
            viewModel.properties.observe(this, onViewModelSubmitList)

            propertiesList.adapter = adapter
        }
    }

    override fun onPropertiesProcessingError() {
        runOnUiThread {
            AlertDialog.Builder(this)
                .setTitle(R.string.alert_title)
                .setMessage(R.string.alert_property_list_error)
                .setNegativeButton(R.string.alert_cancel, null)
                .setPositiveButton(R.string.alert_try_again, {_, _ ->
                    RxSwipeRefreshLayout.refreshing(refreshLayout).accept(true)
                    initList()
                })
                .show()
        }
    }

    override fun getTitleForActionBar(): String = getString(R.string.property_list_title)

    override fun getProperties(page: Int): Observable<List<PropertyListItemEntity>> =
        presenter.getPage(page)

    override fun layoutResourceId(): Int =
        R.layout.activity_property_list

    private val onRefreshesCalled: (Any) -> Unit = {
        RxSwipeRefreshLayout.refreshing(refreshLayout).accept(true)
        adapter = null
        initList()
    }

    private val onPropertyItemClickedSubject : PublishSubject<PropertyListItemEntity> = PublishSubject.create()
    private val onPropertyItemClicked  : Consumer<PropertyListItemEntity> = Consumer {
        startActivity(AppRouter
                        .with(this@PropertyListActivity)
                        .toPropertyDetail(it))
    }

    private val onViewModelSubmitList : Observer<PagedList<PropertyListItemEntity>> = Observer {
        RxSwipeRefreshLayout.refreshing(refreshLayout).accept(false)
        adapter?.submitList(it)
    }

    override fun dispose() {
        super.dispose()
        presenter.dispose()
    }
}
