package br.com.pedronsouza.testegrupozap.presentation.views.propertyList.dataSources

import androidx.lifecycle.MutableLiveData
import androidx.paging.DataSource
import br.com.pedronsouza.testegrupozap.presentation.entities.PropertyListItemEntity
import br.com.pedronsouza.testegrupozap.presentation.views.propertyList.PropertyListView
import io.reactivex.disposables.CompositeDisposable

class PropertyListFactory(val view : PropertyListView, val bag : CompositeDisposable) : DataSource.Factory<Int, PropertyListItemEntity>() {
    val propertyListDataSourceLiveData : MutableLiveData<PropertyListDataSource> = MutableLiveData()

    override fun create(): DataSource<Int, PropertyListItemEntity> {
        val dataSource = PropertyListDataSource(view, bag)
        propertyListDataSourceLiveData.postValue(dataSource)
        return dataSource
    }
}