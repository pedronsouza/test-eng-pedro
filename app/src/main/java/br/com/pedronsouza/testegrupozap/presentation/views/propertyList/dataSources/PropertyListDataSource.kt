package br.com.pedronsouza.testegrupozap.presentation.views.propertyList.dataSources

import androidx.paging.PageKeyedDataSource
import br.com.pedronsouza.testegrupozap.presentation.entities.PropertyListItemEntity
import br.com.pedronsouza.testegrupozap.presentation.views.propertyList.PropertyListView
import io.reactivex.disposables.CompositeDisposable

class PropertyListDataSource(private val view : PropertyListView, val bag : CompositeDisposable) : PageKeyedDataSource<Int, PropertyListItemEntity>() {
    override fun loadInitial(
        params: LoadInitialParams<Int>,
        callback: LoadInitialCallback<Int, PropertyListItemEntity>) {
        bag.add(view.getProperties(1).subscribe( { callback.onResult(it, null, 2)  }, view.getErrorHandler()))
    }

    override fun loadAfter(params: LoadParams<Int>, callback: LoadCallback<Int, PropertyListItemEntity>) {
        val nextPage = params.key + 1
        bag.add(view.getProperties(params.key).subscribe({ callback.onResult(it, nextPage) }, view.getErrorHandler()))
    }

    override fun loadBefore(params: LoadParams<Int>, callback: LoadCallback<Int, PropertyListItemEntity>) {

    }

}