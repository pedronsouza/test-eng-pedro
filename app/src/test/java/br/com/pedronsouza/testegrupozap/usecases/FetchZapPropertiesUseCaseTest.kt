package br.com.pedronsouza.testegrupozap.usecases

import br.com.pedronsouza.testegrupozap.data.datasources.PropertiesDataSource
import br.com.pedronsouza.testegrupozap.data.usecases.properties.FetchZapPropertiesUseCase
import br.com.pedronsouza.testegrupozap.domain.models.*
import io.mockk.every
import io.mockk.mockkClass
import io.mockk.unmockkAll
import io.reactivex.Observable
import org.junit.Before
import org.junit.Test

class FetchZapPropertiesUseCaseTest {
    private val doOnError : (Throwable) -> Unit = {}
    private val invalidGeolocation : PropertyGeoLocation = PropertyGeoLocation(GeoLocationPrecisionEnum.NO_GEOCODE, PropertyLatLng(0.0, 0.0))
    private val validGeolocation : PropertyGeoLocation = PropertyGeoLocation(GeoLocationPrecisionEnum.APPROXIMATE, PropertyLatLng( -23.568704, -46.693419))

    private val addressValidLocation : PropertyAddress = PropertyAddress("", "", validGeolocation)
    private val addressInvalidLocation : PropertyAddress = PropertyAddress("", "", invalidGeolocation)

    private val withMinUsableAreaPrice : PricingInfo = PricingInfo("", 3.500, BusinessTypeEnum.SALE, 0.0)

    private val invalidPriceRent : PricingInfo = PricingInfo("", 3.400, BusinessTypeEnum.RENT, 0.0)
    private val validPriceRent : PricingInfo = PricingInfo("", 3.500, BusinessTypeEnum.RENT, 0.0)

    private val invalidPriceSale : PricingInfo = PricingInfo("", 500.000, BusinessTypeEnum.SALE, 0.0)
    private val validPriceSale : PricingInfo = PricingInfo("", 600.000, BusinessTypeEnum.SALE, 0.0)


    lateinit var propertyWithInvalidGeolocation : Property
    lateinit var propertyWithInvalidPriceRent : Property
    lateinit var propertyWithInvalidPriceSale : Property
    lateinit var propertyWithMinUsableArea : Property
    lateinit var propertyWithoutMinUsableArea : Property
    lateinit var validProperty : Property

    val dataSourceMock = mockkClass(PropertiesDataSource::class)
    val useCase = FetchZapPropertiesUseCase(dataSourceMock)

    @Before
    fun setUp() {
        propertyWithInvalidGeolocation = Property("", false, 3, 3, emptyList(), addressInvalidLocation, 0, 0, withMinUsableAreaPrice)
        propertyWithInvalidPriceRent = Property("", false, 3, 3, emptyList(), addressInvalidLocation, 0, 0, invalidPriceRent)
        propertyWithInvalidPriceSale = Property("", false, 3, 3, emptyList(), addressInvalidLocation, 0, 0, invalidPriceSale)
        propertyWithMinUsableArea = Property("", false, 3, 3, emptyList(), addressInvalidLocation, 0, 0, withMinUsableAreaPrice)
        propertyWithoutMinUsableArea = Property("", false, 3, 3, emptyList(), addressInvalidLocation, 0, 0, validPriceSale)
        validProperty = Property("", false, 3, 3, emptyList(), addressValidLocation, 0, 0, validPriceRent)
        unmockkAll()
    }

    @Test
    fun shouldReturnEmptyListWithInvalidGeolocation() {
        useCase.onError = doOnError

        every { dataSourceMock.fetch() } returns Observable.just(listOf(propertyWithInvalidGeolocation))
        val observableResult = useCase.observableFor().test()

        observableResult.assertComplete()
        observableResult.assertValue(emptyList())
        observableResult.dispose()
    }

    @Test
    fun shouldReturnEmptyListWithInvalidPriceRent() {
        useCase.onError = doOnError

        every { dataSourceMock.fetch() } returns Observable.just(listOf(propertyWithInvalidPriceRent))
        val observableResult = useCase.observableFor().test()

        observableResult.assertComplete()
        observableResult.assertValue(emptyList())
        observableResult.dispose()
    }

    @Test
    fun shouldReturnEmptyListWithInvalidPriceSale() {
        useCase.onError = doOnError

        every { dataSourceMock.fetch() } returns Observable.just(listOf(propertyWithInvalidPriceSale))
        val observableResult = useCase.observableFor().test()

        observableResult.assertComplete()
        observableResult.assertValue(emptyList())
        observableResult.dispose()
    }

    @Test
    fun shouldReturnResultsWithValidProperty() {
        useCase.onError = doOnError

        val expected = listOf(validProperty)

        every { dataSourceMock.fetch() } returns Observable.just(expected)
        val observableResult = useCase.observableFor().test()

        observableResult.assertComplete()
        observableResult.assertValue(expected)
        observableResult.dispose()
    }
}