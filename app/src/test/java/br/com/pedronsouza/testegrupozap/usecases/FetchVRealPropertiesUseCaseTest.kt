package br.com.pedronsouza.testegrupozap.usecases

import br.com.pedronsouza.testegrupozap.data.datasources.PropertiesDataSource
import br.com.pedronsouza.testegrupozap.data.usecases.properties.FetchVRealPropertiesUseCase
import br.com.pedronsouza.testegrupozap.data.usecases.properties.FetchZapPropertiesUseCase
import br.com.pedronsouza.testegrupozap.domain.models.*
import io.mockk.every
import io.mockk.mockkClass
import io.mockk.unmockkAll
import io.reactivex.Observable
import org.junit.Before
import org.junit.Test

class FetchVRealPropertiesUseCaseTest {
    private val doOnError : (Throwable) -> Unit = {}
    private val invalidGeolocation : PropertyGeoLocation = PropertyGeoLocation(GeoLocationPrecisionEnum.NO_GEOCODE, PropertyLatLng(0.0, 0.0))
    private val validGeolocation : PropertyGeoLocation = PropertyGeoLocation(GeoLocationPrecisionEnum.APPROXIMATE, PropertyLatLng( -23.568704, -46.693419))

    private val addressValidLocation : PropertyAddress = PropertyAddress("", "", validGeolocation)
    private val addressInvalidLocation : PropertyAddress = PropertyAddress("", "", invalidGeolocation)

    private val withValidCondoFeePrice: PricingInfo = PricingInfo("", 3.500, BusinessTypeEnum.RENTAL, 350.0)
    private val withInvalidCondoFeePrice: PricingInfo = PricingInfo("", 3.500, BusinessTypeEnum.RENTAL, 1.500)

    private val invalidPriceRent : PricingInfo = PricingInfo("", 5.400, BusinessTypeEnum.RENT, 0.0)
    private val validPriceRent : PricingInfo = PricingInfo("", 3.500, BusinessTypeEnum.RENT, 0.0)

    private val invalidPriceSale : PricingInfo = PricingInfo("", 500.000, BusinessTypeEnum.SALE, 0.0)
    private val validPriceSale : PricingInfo = PricingInfo("", 600.000, BusinessTypeEnum.SALE, 0.0)


    lateinit var propertyWithInvalidGeolocation : Property
    lateinit var propertyWithInvalidPriceRent : Property
    lateinit var propertyWithInvalidPriceSale : Property
    lateinit var propertyWithValidCondoFee : Property
    lateinit var propertyWithInvalidCondoFee : Property
    lateinit var validProperty : Property

    val dataSourceMock = mockkClass(PropertiesDataSource::class)
    val useCase = FetchVRealPropertiesUseCase(dataSourceMock)

    @Before
    fun setUp() {
        propertyWithInvalidGeolocation = Property("", false, 3, 3, emptyList(), addressInvalidLocation, 0, 0, validPriceSale)
        propertyWithInvalidPriceRent = Property("", false, 3, 3, emptyList(), addressInvalidLocation, 0, 0, invalidPriceRent)
        propertyWithInvalidPriceSale = Property("", false, 3, 3, emptyList(), addressInvalidLocation, 0, 0, invalidPriceSale)
        propertyWithValidCondoFee = Property("", false, 3, 3, emptyList(), addressInvalidLocation, 0, 0, withValidCondoFeePrice)
        propertyWithInvalidCondoFee = Property("", false, 3, 3, emptyList(), addressInvalidLocation, 0, 0, withInvalidCondoFeePrice)
        validProperty = Property("", false, 3, 3, emptyList(), addressValidLocation, 0, 0, validPriceRent)
        unmockkAll()
    }

    @Test
    fun shouldReturnEmptyListWithInvalidCondoFee() {
        useCase.onError = doOnError

        every { dataSourceMock.fetch() } returns Observable.just(listOf(propertyWithValidCondoFee))
        val observableResult = useCase.observableFor().test()

        observableResult.assertComplete()
        observableResult.assertValue(emptyList())
        observableResult.dispose()
    }

    @Test
    fun shouldReturnEmptyListWithInvalidGeolocation() {
        useCase.onError = doOnError

        every { dataSourceMock.fetch() } returns Observable.just(listOf(propertyWithInvalidGeolocation))
        val observableResult = useCase.observableFor().test()

        observableResult.assertComplete()
        observableResult.assertValue(emptyList())
        observableResult.dispose()
    }

    @Test
    fun shouldReturnEmptyListWithInvalidPriceRent() {
        useCase.onError = doOnError

        every { dataSourceMock.fetch() } returns Observable.just(listOf(propertyWithInvalidPriceRent))
        val observableResult = useCase.observableFor().test()

        observableResult.assertComplete()
        observableResult.assertValue(emptyList())
        observableResult.dispose()
    }

    @Test
    fun shouldReturnEmptyListWithInvalidPriceSale() {
        useCase.onError = doOnError

        every { dataSourceMock.fetch() } returns Observable.just(listOf(propertyWithInvalidPriceSale))
        val observableResult = useCase.observableFor().test()

        observableResult.assertComplete()
        observableResult.assertValue(emptyList())
        observableResult.dispose()
    }

    @Test
    fun shouldReturnResultsWithValidProperty() {
        useCase.onError = doOnError

        val expected = listOf(validProperty)

        every { dataSourceMock.fetch() } returns Observable.just(expected)
        val observableResult = useCase.observableFor().test()

        observableResult.assertComplete()
        observableResult.assertValue(expected)
        observableResult.dispose()
    }
}