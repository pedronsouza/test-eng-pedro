package br.com.pedronsouza.testegrupozap.data.usecases.properties

import br.com.pedronsouza.testegrupozap.data.datasources.PropertiesDataSource
import br.com.pedronsouza.testegrupozap.domain.models.BusinessTypeEnum
import br.com.pedronsouza.testegrupozap.domain.models.Property
import io.reactivex.Observable

class FetchVRealPropertiesUseCase(dataSource: PropertiesDataSource) : FetchAvailablePropertiesUseCase(dataSource)  {
    override fun observableFor(): Observable<List<Property>> {
        val listObs = super.observableFor()
        return listObs.flatMap {
            Observable.just(it
                .filter(maxCondoFeeRule)
                .filter(minMaxPriceRule)
            )
        }
    }

    private val maxCondoFeeRule : (Property) -> Boolean = {
        var maxCondoFeeFactor = 0.3
        var isElegibleToProvider = true

        if (isInsideBoundingBox(it.address.geoLocation)) {
            maxCondoFeeFactor = 0.5
        }

        if (it.pricingInfos.businessType == BusinessTypeEnum.RENTAL || it.pricingInfos.businessType == BusinessTypeEnum.RENT) {
            val maxCondoFee = it.pricingInfos.price * maxCondoFeeFactor
            isElegibleToProvider = (it.pricingInfos.monthlyCondoFee < maxCondoFee)
        }

        isElegibleToProvider
    }

    private val minMaxPriceRule : (Property) -> Boolean = {item ->
        (item.pricingInfos.price <= 4000 && (item.pricingInfos.businessType == BusinessTypeEnum.RENT || item.pricingInfos.businessType == BusinessTypeEnum.RENTAL)) ||
        (item.pricingInfos.price <= 700000 && item.pricingInfos.businessType == BusinessTypeEnum.SALE)
    }
}