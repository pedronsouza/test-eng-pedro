package br.com.pedronsouza.testegrupozap.data.net

import br.com.pedronsouza.testegrupozap.data.net.services.PropertyServices
import retrofit2.Retrofit

class Api(private val provider : Retrofit) {
    val property by lazy { provider.create(PropertyServices::class.java) }
}