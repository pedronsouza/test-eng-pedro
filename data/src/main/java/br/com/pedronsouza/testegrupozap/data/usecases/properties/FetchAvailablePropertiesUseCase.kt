package br.com.pedronsouza.testegrupozap.data.usecases.properties

import br.com.pedronsouza.testegrupozap.data.datasources.PropertiesDataSource
import br.com.pedronsouza.testegrupozap.data.usecases.BaseUseCase
import br.com.pedronsouza.testegrupozap.domain.models.Property
import br.com.pedronsouza.testegrupozap.domain.models.PropertyGeoLocation
import io.reactivex.Observable

abstract class FetchAvailablePropertiesUseCase(private val dataSource: PropertiesDataSource) : BaseUseCase<List<Property>>() {
    private val boundingBoxGrupoZap  = arrayOf(-46.693419, -23.568704, -46.641146, -23.546686)

    lateinit var onError : (Throwable) -> Unit
    override fun doOnError(): (Throwable) -> Unit = onError

    override fun observableFor(): Observable<List<Property>> =
        dataSource.fetch()
            .flatMap { Observable.just(it.filter(validGeoLocationRule)) }


    private val validGeoLocationRule : (Property) -> Boolean = {
        it.address.geoLocation.location.lat != 0.toDouble() &&
        it.address.geoLocation.location.lon != 0.toDouble()
    }

    protected fun isInsideBoundingBox(location : PropertyGeoLocation) : Boolean {
        val lat = location.location.lat
        val lon = location.location.lon
        return (lon >= boundingBoxGrupoZap[0] && // min lon
                lat >= boundingBoxGrupoZap[1] && // min lat
                lon <= boundingBoxGrupoZap[1] && // max lon
                lat <= boundingBoxGrupoZap[2]) // max lat
    }
}