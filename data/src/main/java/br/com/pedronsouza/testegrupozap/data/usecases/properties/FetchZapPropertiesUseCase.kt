package br.com.pedronsouza.testegrupozap.data.usecases.properties

import br.com.pedronsouza.testegrupozap.data.datasources.PropertiesDataSource
import br.com.pedronsouza.testegrupozap.domain.models.BusinessTypeEnum
import br.com.pedronsouza.testegrupozap.domain.models.Property
import io.reactivex.Observable

class FetchZapPropertiesUseCase(dataSource: PropertiesDataSource) : FetchAvailablePropertiesUseCase(dataSource)  {
    override fun observableFor(): Observable<List<Property>> {
        val listObs = super.observableFor()
        return listObs.flatMap {
            Observable.just(it
                    .filter(minUsableAreaRule)
                    .filter(minMaxPriceRule)
            )
        }
    }

    private val minUsableAreaRule : (Property) -> Boolean = {
        var minUsableAreaValue = 3.500
        var isElegibleToProvider = true

        if (isInsideBoundingBox(it.address.geoLocation)) {
            minUsableAreaValue = (minUsableAreaValue - (minUsableAreaValue * 0.1))
        }

        if (it.pricingInfos.businessType == BusinessTypeEnum.SALE) {
            isElegibleToProvider = (it.usableAreas > 0) && (it.usableAreas > minUsableAreaValue)
        }

        isElegibleToProvider
    }

    private val minMaxPriceRule : (Property) -> Boolean = { item ->
        (item.pricingInfos.price >= 3.500 && (item.pricingInfos.businessType == BusinessTypeEnum.RENT || item.pricingInfos.businessType == BusinessTypeEnum.RENTAL)) ||
        (item.pricingInfos.price >= 600000 && item.pricingInfos.businessType == BusinessTypeEnum.SALE)
    }

}