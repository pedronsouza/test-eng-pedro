package br.com.pedronsouza.testegrupozap.data.usecases

import br.com.pedronsouza.testegrupozap.domain.data.usecases.UseCase
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

abstract class BaseUseCase<T> : UseCase<T>() {
    override fun dispose() {
        disposable?.dispose()
    }

    override fun execute(consumer: (T) -> Unit) {
        disposable = observableFor()
            .subscribeOn(Schedulers.computation())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnError(doOnError())
            .subscribe(consumer)
    }
}