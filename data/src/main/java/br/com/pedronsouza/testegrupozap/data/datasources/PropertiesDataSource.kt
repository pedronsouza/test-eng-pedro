package br.com.pedronsouza.testegrupozap.data.datasources

import br.com.pedronsouza.testegrupozap.data.datasources.core.RemoteBaseDataSource
import br.com.pedronsouza.testegrupozap.data.net.Api
import br.com.pedronsouza.testegrupozap.domain.models.Property
import io.reactivex.Observable

class PropertiesDataSource(api : Api) : RemoteBaseDataSource(api) {
    fun fetch() : Observable<List<Property>> =
        api.property.all()
}