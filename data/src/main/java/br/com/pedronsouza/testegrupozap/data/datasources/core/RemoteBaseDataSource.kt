package br.com.pedronsouza.testegrupozap.data.datasources.core

import br.com.pedronsouza.testegrupozap.data.net.Api

abstract class RemoteBaseDataSource(protected val api : Api)