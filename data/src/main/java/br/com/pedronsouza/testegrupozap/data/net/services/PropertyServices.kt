package br.com.pedronsouza.testegrupozap.data.net.services

import br.com.pedronsouza.testegrupozap.domain.models.Property
import io.reactivex.Observable
import retrofit2.http.GET

interface PropertyServices {
    @GET("sources/source-1.json")
    fun all() : Observable<List<Property>>
}