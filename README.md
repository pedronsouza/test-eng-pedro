# Como Rodar os Testes

```bash
cd /path/to/repo
./gradlew cAT
```

# Como Rodar a Aplicação
### Dependencias
- Android SDK v28
- Android Studio 3.*

Basta abrir a aplicação pelo arquivo `buid.gradle` raiz ou então:

```bash
cd /path/to/repo
./gradlew installDebug
```