package br.com.pedronsouza.testegrupozap.domain.models

class PropertyGeoLocation(val precision : GeoLocationPrecisionEnum, val location: PropertyLatLng) {
}