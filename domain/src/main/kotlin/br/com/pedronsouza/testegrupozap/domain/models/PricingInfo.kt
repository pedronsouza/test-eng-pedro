package br.com.pedronsouza.testegrupozap.domain.models

data class PricingInfo(val yearlyIptu : String,
                       val price : Double,
                       val businessType : BusinessTypeEnum,
                       val monthlyCondoFee : Double)