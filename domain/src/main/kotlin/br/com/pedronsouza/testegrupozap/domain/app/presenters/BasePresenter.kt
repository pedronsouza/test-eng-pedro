package br.com.pedronsouza.testegrupozap.domain.app.presenters

import br.com.pedronsouza.testegrupozap.domain.app.views.BaseView
import io.reactivex.disposables.CompositeDisposable

abstract class BasePresenter<T : BaseView> {
    protected val disposableBag : CompositeDisposable = CompositeDisposable()
    protected lateinit var view : T

    open fun dispose() {
        disposableBag.dispose()
    }
    open fun onViewCreated(view : T) {
        this.view = view
    }
}