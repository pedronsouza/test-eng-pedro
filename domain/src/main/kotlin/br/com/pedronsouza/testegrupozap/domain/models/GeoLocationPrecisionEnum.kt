package br.com.pedronsouza.testegrupozap.domain.models

import com.google.gson.annotations.SerializedName

enum class GeoLocationPrecisionEnum(val value : String) {
    @SerializedName("NO_GEOCODE")
    NO_GEOCODE("NO_GEOCODE"),

    @SerializedName("ROOFTOP")
    ROOFTOP("ROOFTOP"),

    @SerializedName("RANGE_INTERPOLATED")
    RANGE_INTERPOLATED("RANGE_INTERPOLATED"),

    @SerializedName("GEOMETRIC_CENTER")
    GEOMETRIC_CENTER("GEOMETRIC_CENTER"),

    @SerializedName("APPROXIMATE")
    APPROXIMATE("APPROXIMATE")
}