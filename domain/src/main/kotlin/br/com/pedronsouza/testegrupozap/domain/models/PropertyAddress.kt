package br.com.pedronsouza.testegrupozap.domain.models

data class PropertyAddress(val city : String, val neighborhood : String, val geoLocation: PropertyGeoLocation)