package br.com.pedronsouza.testegrupozap.domain.app.views

import io.reactivex.disposables.CompositeDisposable

interface BaseView {
    fun onViewsBinded()
    fun getTitleForActionBar() : String
    fun getBag() : CompositeDisposable
    fun dispose()
}