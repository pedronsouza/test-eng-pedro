package br.com.pedronsouza.testegrupozap.domain.models

data class Property(val id : String,
                    val owner : Boolean,
                    val usableAreas : Int,
                    val parkingSpaces : Int,
                    val images : List<String>,
                    val address : PropertyAddress,
                    val bathrooms : Int,
                    val bedrooms : Int,
                    val pricingInfos : PricingInfo) {
}