package br.com.pedronsouza.testegrupozap.domain.models

data class PropertyLatLng(val lat : Double, val lon : Double)