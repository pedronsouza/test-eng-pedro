package br.com.pedronsouza.testegrupozap.domain.data.usecases

import io.reactivex.Observable
import io.reactivex.disposables.Disposable

abstract class UseCase<T> {
    protected var disposable : Disposable? = null
    abstract fun observableFor() : Observable<T>
    abstract fun execute(consumer : (T) -> Unit)
    abstract fun dispose()
    abstract fun doOnError() : (Throwable) -> Unit
}