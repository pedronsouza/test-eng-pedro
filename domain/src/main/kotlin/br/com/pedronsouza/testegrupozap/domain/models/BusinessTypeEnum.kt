package br.com.pedronsouza.testegrupozap.domain.models

import com.google.gson.annotations.SerializedName

enum class BusinessTypeEnum(val value : String) {
    @SerializedName("RENT")
    RENT("RENT"),
    @SerializedName("SALE")
    SALE("SALE"),
    @SerializedName("RENTAL")
    RENTAL("RENTAL")
}